import { StyleSheet, Text, View, SafeAreaView, Image } from "react-native";
import React from "react";
import COLORS from "../../consts/colors";
import { PrimaryButton } from "../components/Button";

const OnBoardingScreen = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>
      <View style={{ height: 400 }}>
        <Image
          style={{ width: "100%", resizeMode: "contain", top: -150 }}
          source={require("../../assets/onboardImage.png")}
        />
      </View>
      <View style={styles.textContainer}>
        <View>
          <Text
            style={{ fontSize: 30, fontWeight: "bold", textAlign: "center" }}
          >
            Delicious Food
          </Text>
          <Text
            style={{
              marginTop: 20,
              fontSize: 18,
              color: COLORS.grey,
              textAlign: "center",
            }}
          >
            We help you find the best and delicious food
          </Text>
        </View>
        <View style={styles.indicatorContainer}>
          <View style={styles.currentIndicator} />
          <View style={styles.indicator} />
          <View style={styles.indicator} />
        </View>
        <PrimaryButton
          onPress={() => navigation.navigate("Home")}
          title="Get Started"
        />
      </View>
    </SafeAreaView>
  );
};

export default OnBoardingScreen;

const styles = StyleSheet.create({
  textContainer: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "space-between",
    paddingBottom: 40,
  },
  indicatorContainer: {
    flex: 1,
    height: 50,
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
  },
  currentIndicator: {
    height: 12,
    width: 30,
    backgroundColor: COLORS.primary,
    borderRadius: 10,
    marginHorizontal: 5,
  },
  indicator: {
    height: 12,
    width: 12,
    backgroundColor: COLORS.grey,
    borderRadius: 6,
    marginHorizontal: 6,
  },
});
