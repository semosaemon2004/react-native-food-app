import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import COLORS from "../../consts/colors";
import { SecondaryButton } from "../components/Button";

const DetailsScreen = ({ navigation, route }) => {
  const item = route.params;
  return (
    <SafeAreaView style={{ backgroundColor: COLORS.white }}>
      <View style={styles.header}>
        <Icon name="arrow-back-ios" size={28} onPress={navigation.goBack} />
        <Text style={{ fontSize: 20, fontWeight: "bold" }}>Details</Text>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            height: 280,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Image source={item.image} style={{ height: 220, width: 220 }} />
        </View>
        <View style={styles.details}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Text
              style={{ fontSize: 20, fontWeight: "bold", color: COLORS.white }}
            >
              {item.name}
            </Text>
            <View style={styles.iconContainer}>
              <Icon name="favorite-border" size={25} color={COLORS.primary} />
            </View>
          </View>
          <Text style={styles.detailsText}>
            Pepperoni pizza is a favorite in many parts of the world. It
            features a layer of tomato sauce, mozzarella cheese, and slices of
            pepperoni, which are a type of spicy Italian salami. Hawaiian:
            Hawaiian pizza is a controversial variety that combines sweet and
            savory flavors. It typically includes tomato sauce, mozzarella
            cheese, ham or Canadian bacon, and pineapple chunks.
          </Text>
          <View style={{ marginTop: 40, marginBottom: 40 }}>
            <SecondaryButton title="Add To Card" />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default DetailsScreen;

const styles = StyleSheet.create({
  header: {
    paddingVertical: 20,
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 20,
  },
  details: {
    paddingHorizontal: 20,
    paddingTop: 40,
    paddingBottom: 60,
    backgroundColor: COLORS.primary,
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
  },
  iconContainer: {
    height: 50,
    width: 50,
    backgroundColor: COLORS.white,
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
  },
  detailsText: {
    marginTop: 10,
    lineHeight: 22,
    fontSize: 15,
    color: COLORS.white,
  },
});
